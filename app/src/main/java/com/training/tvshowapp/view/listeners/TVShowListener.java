package com.training.tvshowapp.view.listeners;

import com.training.tvshowapp.models.TVShow;

public interface TVShowListener {
    void onTVShowItemClicked(TVShow tvShow, int position);
}
